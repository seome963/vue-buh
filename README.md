### <span style="color:blue">Materialize + Vue + Firebase</span>

# Demo

https://vue-buh-app.web.app/

For authorization:<br />
email: demovuebuh@gmail.com<br />
password: demo1234<br />

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
