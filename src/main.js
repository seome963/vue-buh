import Vue from 'vue'
import VueMeta from 'vue-meta'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import currencyFilter from '@/filters/currency.filter'
import messagesPlugin from '@/utils/messages.plugin'
import titlePlugin from '@/utils/title.plugin'
import localizeFilter from '@/filters/localize.filter'
import tooltipDirective from '@/directives/tooltip.directive'
import Loader from '@/components/app/Loader'
import Paginate from 'vuejs-paginate'
import './registerServiceWorker'
import "materialize-css/dist/js/materialize.min"

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'




Vue.config.productionTip = false;
Vue.use(Vuelidate);
Vue.use(VueMeta);
Vue.use(messagesPlugin);
Vue.use(titlePlugin);
Vue.filter('date', dateFilter);
Vue.filter('currency', currencyFilter);
Vue.filter('localize', localizeFilter);
Vue.directive('tooltip', tooltipDirective);
Vue.component('Loader', Loader);
Vue.component('Paginate', Paginate);

firebase.initializeApp({
    apiKey: "AIzaSyDh7QoHWgOB0VKw6nPwZxzfWzG1mbN84Ro",
    authDomain: "vue-buh-app.firebaseapp.com",
    projectId: "vue-buh-app",
    storageBucket: "vue-buh-app.appspot.com",
    messagingSenderId: "595838488575",
    appId: "1:595838488575:web:f3fa33760a5a84b57ba1ca"
});

let app;

firebase.auth().onAuthStateChanged(() => {
    if (!app) {
        app = new Vue({
            router,
            store,
            render: h => h(App)
        }).$mount('#app')
    }
})