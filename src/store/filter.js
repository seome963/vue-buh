import firebase from 'firebase/app'

export default {
    actions: {
        async updateFilter({
            dispatch,
            commit
        }, filter) {
            try {
                const uid = await dispatch('getUid');
                return await firebase.database().ref(`/users/${uid}/filters`).update(filter);
            } catch (e) {
                commit('setError', e)
                throw e
            }
        },
        async fetchFilter({
            dispatch,
            commit
        }) {
            try {
                const uid = await dispatch('getUid');
                let filter = (await firebase.database().ref(`/users/${uid}/filters`).once('value')).val() || {};
                return filter;

            } catch (e) {
                commit('setError', e);
                throw e
            }
        },
        async createFilter({
            dispatch,
            commit
        }) {
            try {
                const uid = await dispatch('getUid');
                await firebase.database().ref(`/users/${uid}/filters`).set({
                    from: "",
                    to: "",
                    categoryId: "All",
                });

            } catch (e) {
                commit('setError', e);
                throw e
            }
        },
    }
}