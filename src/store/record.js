import firebase from 'firebase/app'

export default {
    actions: {
        async createRecord({
            dispatch,
            commit
        }, record) {
            try {
                const uid = await dispatch('getUid');
                return await firebase.database().ref(`/users/${uid}/records`).push(record);
            } catch (e) {
                commit('setError', e)
                throw e
            }
        },
        async fetchRecords({
            dispatch,
            commit
        }) {
            try {
                const uid = await dispatch('getUid');
                const records = (await firebase.database().ref(`/users/${uid}/records`).once('value')).val() || {}
                return Object.keys(records).map(key => ({
                    ...records[key],
                    id: key
                }))
            } catch (e) {
                commit('setError', e);
                throw e
            }
        },
        async fetchRecordById({
            dispatch,
            commit
        }, id) {
            try {
                const uid = await dispatch('getUid');
                const record = (await firebase.database().ref(`/users/${uid}/records`).child(id).once('value')).val() || {}
                return {
                    ...record,
                    id
                }
            } catch (e) {
                commit('setError', e);
                throw e
            }
        },
        async removeRecord({
            commit,
            dispatch
        }, {
            id
        }) {
            try {
                const uid = await dispatch('getUid');
                await firebase.database().ref(`/users/${uid}/records/${id}`).remove();
            } catch (e) {
                commit('setError', e);
                throw e
            }
        },
        async removeRecordAllIdCategory({
            commit,
            dispatch
        }, {
            id
        }) {
            try {
                const uid = await dispatch('getUid');
                await id.map(r => firebase.database().ref(`/users/${uid}/records/${r}`).remove());
            } catch (e) {
                commit('setError', e);
                throw e
            }
        },
        async updateRecord({
            dispatch,
            commit
        }, record) {
            console.log(record)
            try {
                const uid = await dispatch('getUid');
                await firebase.database().ref(`/users/${uid}/records/${record.id}`).update(record);
            } catch (e) {
                commit('setError', e);
                throw e
            }
        },
    }
}