export default {
    "logout": "Вы вышли из системы",
    "login": "Для начала войдите в систему",
    "auth/wrong-password": "Не верный пароль",
    "auth/user-not-found": "Пользователь не найден",
    "auth/email-already-in-use": "Email уже занят"
}